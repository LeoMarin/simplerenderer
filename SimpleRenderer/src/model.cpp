#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "model.h"

Model::Model(const char* fileName)
	: m_VertexPositions(), m_UV(), m_Normals(), m_Faces(), m_Texture()
{
	// open .obj file
	std::ifstream objFile(fileName);
	if (!objFile.is_open())
	{
		std::cout << "OBJ file not opened correctly" << std::endl;
		return;
	}

	// read .obj line by line
	std::string line;
	while (!objFile.eof())
	{
		// read line
		std::getline(objFile, line);
		std::istringstream iss(line.c_str());

		// get prefix
		std::string prefix;
		iss >> prefix;

		// read vertex
		if (prefix == "v")
		{
			Vec3f temp;
			iss >> temp.x >> temp.y >> temp.z;
			m_VertexPositions.push_back(temp);
		}
		// read texture coordinates
		else if (prefix == "vt")
		{
			Vec2f temp;
			iss >> temp.x >> temp.y;
			m_UV.push_back(temp);
		}
		// read normal
		else if (prefix == "vn")
		{
			Vec3f temp;
			iss >> temp.x >> temp.y >> temp.z;
			m_Normals.push_back(temp);
		}
		// read face
		else if (prefix == "f")
		{
			// ever face is described by 3 vertices
			// vertices consist of position, uv and normal
			// each line looks like: *** f pos/uv/norm pos/uv/norm pos/uv/norm ***

			FaceStruct tempFace;
			std::vector<FaceStruct> tempVector;
			char trash;

			// read 3 vertices in face
			for (int i = 0; i < 3; i++)
			{
				iss >> tempFace.positionIndex >> trash >> tempFace.uvIndex >> trash >> tempFace.normalIndex;

				// in wavefront obj all indices start at 1, not 0
				tempFace.positionIndex--;
				tempFace.uvIndex--;
				tempFace.normalIndex--;

				tempVector.push_back(tempFace);
			}
			// store current face in m_Faces
			m_Faces.push_back(tempVector);
		}
	}
}


int Model::NumberOfVertices() 
{ 
	return m_VertexPositions.size(); 
}

int Model::NumberOfFaces() 
{ 
	return m_Faces.size(); 
}

Vec3f Model::VertexPosition(int i) 
{ 
	return m_VertexPositions[i]; 
}

std::vector<FaceStruct> Model::Face(int i) 
{ 
	return m_Faces[i]; 
}

bool Model::LoadTexture(const char* fileName)
{
	bool b = m_Texture.read_tga_file(fileName);
	m_Texture.flip_vertically();
	return b;
}

Vec2i Model::UV(int ithFace, int nthVertex)
{
	int i = m_Faces[ithFace][nthVertex].uvIndex;
	return Vec2i(m_UV[i].x * m_Texture.get_width(), m_UV[i].y * m_Texture.get_height());
}

TGAColor Model::TextureColor(Vec2i uv)
{
	return m_Texture.get(uv.x, uv.y);
}

Vec3f Model::VertexNormal(int ithFace, int nthVertex)
{
	int i = m_Faces[ithFace][nthVertex].normalIndex;
	return m_Normals[i];
}
