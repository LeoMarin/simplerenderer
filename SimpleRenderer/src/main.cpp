#include <iostream>

#include "tgaimage.h"
#include "model.h"
#include "deprecated.h"
#include "Matrix.h"

// draw triangle line by line
void Triangle(Vec3i t[3], Vec2i uv[3], float intensity[3], TGAImage& image, Model* model, float* zBuffer)
{
    // order vertices by y ascending 
    if (t[0].y > t[1].y) { std::swap(t[0], t[1]); std::swap(uv[0], uv[1]); std::swap(intensity[0], intensity[1]); };
    if (t[0].y > t[2].y) { std::swap(t[0], t[2]); std::swap(uv[0], uv[2]); std::swap(intensity[0], intensity[2]); };
    if (t[1].y > t[2].y) { std::swap(t[1], t[2]); std::swap(uv[1], uv[2]); std::swap(intensity[1], intensity[2]); };
    
    // height of the segment with y difference
    int totalHeight = t[2].y - t[0].y;
    // height of the bottom part
    int firstSegmentHeight = t[1].y - t[0].y;
    // height of the upper part
    int secondSegmentHeight = t[2].y - t[1].y;
    
    // variables that keep track of current half
    int currentSegmentHeight = firstSegmentHeight;
    int segmentIndex = 0; // 0 if bottom, 1 if upper
    int lineNumber = 0; // line number in current part

    // draw triangle line by line
    for (int i = 0; i < totalHeight; i++)
    {
        // check if first segment is completed
        if (i == firstSegmentHeight)
        {
            currentSegmentHeight = secondSegmentHeight;
            lineNumber = 0;
            segmentIndex = 1;
        }

        // current percentage for difference vector
        float alpha = (float)i / totalHeight;
        float beta = (float)lineNumber / currentSegmentHeight;
        lineNumber++;

        // current line begining and ending
        // starting vector + difference vector * percentage
        // difference vector = ending vector - starting vector
        Vec3i a = t[0] + Vec3f(t[2] - t[0]) * alpha;
        Vec3i b = t[segmentIndex] + Vec3f(t[segmentIndex + 1] - t[segmentIndex]) * beta;

        // texture coodrinates for line begining and ending
        Vec2i uvA = uv[0] + (uv[2] - uv[0]) * alpha;
        Vec2i uvB = uv[segmentIndex] + (uv[segmentIndex + 1] - uv[segmentIndex]) * beta;

        // intensity for line begining and ending
        float intensityA = intensity[0] + (intensity[2] - intensity[0]) * alpha;
        float intensityB = intensity[segmentIndex] + (intensity[segmentIndex + 1] - intensity[segmentIndex]) * beta;

        // order vertices by x ascending
        if (a.x > b.x) { std::swap(a, b); std::swap(uvA, uvB); std::swap(intensityA, intensityB); }
        int lineWidth = b.x - a.x;
        // draw horizontal line pixel by pixel
        for (int j = 0; j <= lineWidth; j++)
        {
            // current percentage for difference vector
            float gama = (j == lineWidth) ? 1 : (float)j / lineWidth;

            // current pixel position, uv coodrinates and intensity
            // current = starting + difference * percentage
            // difference = end - sart
            Vec3i pixelPosition = Vec3f(a) + Vec3f(b - a) * gama;
            Vec2i pixelUv = uvA + (uvB - uvA) * gama;
            float pixelIntensity = intensityA + (intensityB - intensityA) * gama;
            
            // skip pixel out of bound
            if (pixelPosition.x >= image.get_width() || pixelPosition.x < 0 ||
                pixelPosition.y >= image.get_height() || pixelPosition.y < 0)
                continue;

            // calculate 1D array index form 2D coordinates
            int zBufferIndex = pixelPosition.x + pixelPosition.y * image.get_width();
            
            // if current pixel is in front of existing pixel (or on empty location), draw on top of it
            if (zBuffer[zBufferIndex] < pixelPosition.z)
            {
                zBuffer[zBufferIndex] = pixelPosition.z;
                TGAColor pixelColor = model->TextureColor(pixelUv);
                image.set(pixelPosition.x, pixelPosition.y, pixelColor * pixelIntensity);
            }
        }
    }
}


void DrawModel(TGAImage& image, float* zBuffer, Model* model, WorldSetting worldSettings)
{
    Vec3f cameraVector = worldSettings.eyeVector - worldSettings.center;
    Matrix projection = Matrix::ProjectionMatrix(cameraVector.norm());
    Matrix viewPort = Matrix::ViewPort(image.get_width() / 8, image.get_height() / 8, image.get_width() * 3 / 4, image.get_height() * 3 / 4);
    Matrix transformationMatrix = viewPort * projection;
    transformationMatrix = transformationMatrix * Matrix::LookAt(worldSettings.eyeVector, worldSettings.center, Vec3f::up());

    // cycle through entire object face by face
    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        // get current face
        std::vector<FaceStruct> face = model->Face(i);
        
        // get screen position, world positions and intensity of 3 vertices describing current face
        Vec3i screenPos[3]; // pixel positions on final screen
        Vec3f worldPos[3]; // used for calculating face normal
        Vec2i uv[3]; // uv coordinates
        float intensity[3]; // intensity for each vertex

        // visit each vertex of current face and get information from .obj file
        for (int j = 0; j < 3; j++)
        {
            Vec3f vertexPos = model->VertexPosition(face[j].positionIndex);
            worldPos[j] = vertexPos;
            screenPos[j] = transformationMatrix * vertexPos;
            intensity[j] = model->VertexNormal(i, j) * worldSettings.lightDirection;
            uv[j] = model->UV(i, j);
        }
        Triangle(screenPos, uv, intensity, image, model, zBuffer);
    }
}

int main()
{
    const int width = 400;
    const int height = 400;

    TGAImage image(width, height, TGAImage::RGB);

    // load model and texture;
    Model* model = new Model("./obj/african_head.obj");
    model->LoadTexture("./obj/african_head_texture.tga");

    // create z-buffer
    int bufferSize = image.get_height() * image.get_width();
    float* zBuffer = new float[bufferSize];
    for (int i = 0; i < bufferSize; i++) zBuffer[i] = -height*2;

    // setup world settings
    WorldSetting worldSetting;
    worldSetting.lightDirection = Vec3f(1, -1, 1).normalize();;
    worldSetting.eyeVector = Vec3f(1, 1, 3);
    worldSetting.center = Vec3f(0, 0, 0);

    DrawModel(image, zBuffer, model, worldSetting);

    delete model;

    image.flip_vertically();
    image.write_tga_file("./images/output.tga"); 
    return 0;
}