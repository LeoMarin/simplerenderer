#pragma once

#include "Vector.h"

class HomogeneousVector
{
public:
	HomogeneousVector() : x(0), y(0), z(0), h(0) {}
	HomogeneousVector(const Vec3f& v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
		h = 1;
	}

	Vec3f Vec3FromHomogeneus()
	{
		return Vec3f(
			(*this).x / (*this).h,
			(*this).y / (*this).h,
			(*this).z / (*this).h
		);
	}

	float& operator[](int i)
	{
		switch (i)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		case 3:
			return h;
		default:
			break;
		}
	}
private:
	float x, y, z, h;
};

class Matrix
{
public:
	Matrix()
	{
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				m_Data[i][j] = 0;
	}
	static Matrix Identity()
	{
		Matrix temp;
		for (int i = 0; i < 4; i++)
			temp.m_Data[i][i] = 1;
		return temp;
	}

	static Matrix ProjectionMatrix(float cameraNormal)
	{
		Matrix temp = Identity();
		temp.m_Data[3][2] = -1 / cameraNormal;
		return temp;
	}

	static Matrix ViewPort(int x, int y, int w, int h)
	{
		Matrix m = Identity();
		m[0][3] = x + w / 2.f;
		m[1][3] = y + h / 2.f;
		m[2][3] = 255 / 2.f;

		m[0][0] = w / 2.f;
		m[1][1] = h / 2.f;
		m[2][2] = 255 / 2.f;
		return m;
	}

	float* operator[](int i) { return m_Data[i]; }

	Matrix operator *(const Matrix& other)
	{
		Matrix temp;
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				temp[i][j] = 0.f;
				for (int k = 0; k < 4; k++)
				{
					temp[i][j] += this->m_Data[i][k] * other.m_Data[k][j];
				}
			}
		}
		return temp;
	}

	HomogeneousVector operator *(HomogeneousVector& vec)
	{
		HomogeneousVector temp;
		for (int i = 0; i < 4; i++)
		{
			temp[i] = 0;
			for (int j = 0; j < 4; j++)
			{
				temp[i] += vec[j] * (*this)[i][j];
			}
		}
		return temp;
	}

	Vec3f operator*(Vec3f& v)
	{
		HomogeneousVector hv(v);
		hv = (*this) * hv;
		return hv.Vec3FromHomogeneus();
	}
	
	static Matrix LookAt(Vec3f eye, Vec3f center, Vec3f up)
	{
		Vec3f z = (eye - center).normalize();
		Vec3f x = (up ^ z).normalize();
		Vec3f y = (z ^ x).normalize();
		Matrix Minv = Identity();
		Matrix Tr = Identity();
		for (int i = 0; i < 3; i++)
		{
			Minv[0][i] = x[i];
			Minv[1][i] = y[i];
			Minv[2][i] = z[i];
			Tr[i][3] = -center[i];
		}
		Matrix ModelView = Minv * Tr;
		return ModelView;
	}

private:
	float m_Data[4][4];
};

struct WorldSetting
{
	Vec3f lightDirection;
	Vec3f eyeVector;
	Vec3f center;
};