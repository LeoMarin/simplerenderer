#pragma once

#include <vector>
#include "Vector.h"
#include "tgaimage.h"

struct FaceStruct
{
	int positionIndex;
	int uvIndex;
	int normalIndex;
};

class Model
{
public:
	Model(const char* fileName);
	Vec3f VertexPosition(int i);
	
	Vec2i UV(int ithFace, int nthVertex);
	TGAColor TextureColor(Vec2i uv);

	Vec3f VertexNormal(int ithFace, int nthVertex);	

	std::vector<FaceStruct> Face(int i);

	int NumberOfVertices();
	int NumberOfFaces();
	bool LoadTexture(const char* fileName);

private:

private:
	std::vector<Vec3f> m_VertexPositions;
	std::vector<Vec2f> m_UV;
	std::vector<Vec3f> m_Normals;
	TGAImage m_Texture;

	// vector of faces
	// each face looks like: [ {posI, uvI, normI}, {posI, uvI, normI}, {posI, uvI, normI} ]
	std::vector<std::vector<FaceStruct>> m_Faces;
};