#pragma once

#include "tgaimage.h"
#include "model.h"
#include "Matrix.h"

/* 
* ******************************************** 
*                   STEP 1 
* ********************************************
*/

// draw line without considering z component
void Line2D(Vec2i t0, Vec2i t1, TGAImage& image, const TGAColor& color)
{
    // order vertices by x ascending
    if (t0.x > t1.x)
    {
        std::swap(t0, t1);
    }

    // if line is steep, transpose it
    bool steep = false;
    if (std::abs(t0.x - t1.x) < std::abs(t0.y - t1.y))
    {
        std::swap(t0.x, t0.y);
        std::swap(t1.x, t1.y);
        steep = true;
    }

    // calculate slope
    float y = t0.y;
    float dy = (float)(t1.y - t0.y) / (t1.x - t0.x);

    // draw line pixel by pixel
    for (int x = t0.x; x <= t1.x; x++)
    {
        if (steep)
            image.set(y + 0.5, x, color);
        else
            image.set(x, y + 0.5, color);
        y += dy;
    }
}

/*
* ********************************************
*                   STEP 2
* ********************************************
*/

// draw wireframe model using Line2D
void DrawWireframeModel(TGAImage& image, const TGAColor& color)
{
    Model* model = new Model("./obj/african_head.obj");

    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        // get current face
        std::vector<FaceStruct> face = model->Face(i);
        for (int j = 0; j < 3; j++)
        {
            // get positions of 2 vertices and convert to Vec2
            Vec3f t0(model->VertexPosition(face[j].positionIndex));
            Vec3f t1(model->VertexPosition(face[(j + 1) % 3].positionIndex));

            // calculate relative position on screen
            int x0 = (t0.x + 1) * image.get_width() / 2;
            int y0 = (t0.y + 1) * image.get_height() / 2;
            int x1 = (t1.x + 1) * image.get_width() / 2;
            int y1 = (t1.y + 1) * image.get_height() / 2;

            Line2D({ x0, y0 }, { x1, y1 }, image, color);
        }
    }

    delete model;
}

/*
* ********************************************
*                   STEP 4
* ********************************************
*/

// draw triangle without considering z component
void Triangle2D(Vec3i t0, Vec3i t1, Vec3i t2, TGAImage& image, const TGAColor& color)
{
    // order vertices by y ascending 
    if (t0.y > t1.y) std::swap(t0, t1);
    if (t0.y > t2.y) std::swap(t0, t2);
    if (t1.y > t2.y) std::swap(t1, t2);

    float a = t0.x; // x position on shorter side
    float b = t0.x; // x position on longer side

    if (t0.y == t1.y) a = t1.x;

    // calculate longer and shorter slope
    float alpha = (t0.y != t1.y) ? (t1.x - t0.x) / (float)(t1.y - t0.y) : 0;
    float beta = (t0.y != t2.y) ? (t2.x - t0.x) / (float)(t2.y - t0.y) : 0;

    // draw bottom half
    for (int y = t0.y; y <= t1.y; y++)
    {
        Line2D({ (int)a, y }, { (int)b, y }, image, color);
        a += alpha;
        b += beta;
    }

    a -= alpha;
    b -= beta;

    // calculate new shorter slope
    alpha = (t1.y != t2.y) ? (t2.x - t1.x) / (float)(t2.y - t1.y) : 0;

    // draw upper half
    for (int y = t1.y; y <= t2.y; y++)
    {
        Line2D({ (int)a, y }, { (int)b, y }, image, color);
        a += alpha;
        b += beta;
    }
}

/*
* ********************************************
*                   STEP 5
* ********************************************
*/

// draw randomly colored model using Triangle2D
void DrawFlatShadedModel(TGAImage& image)
{
    Model* model = new Model("./obj/african_head.obj");
    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        std::vector<FaceStruct> face = model->Face(i);
        Vec3i screenPos[3];
        for (int j = 0; j < 3; j++)
        {
            Vec3f vertexPos = model->VertexPosition(face[j].positionIndex);
            screenPos[j] = Vec3i((vertexPos.x + 1) * image.get_width() / 2, (vertexPos.y + 1) * image.get_height() / 2, 0);
        }
        Triangle2D(screenPos[0], screenPos[1], screenPos[2], image, TGAColor(rand() % 255, rand() % 255, rand() % 255, 255));
    }
    delete model;
}

/*
* ********************************************
*                   STEP 6
* ********************************************
*/

// draw model with illumination from single direction
void DrawIlluminatedModel(TGAImage& image)
{
    Model* model = new Model("./obj/african_head.obj");
    Vec3f light_dir(0, 0, -1);

    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        std::vector<FaceStruct> face = model->Face(i);
        Vec3i screenPos[3];
        Vec3f worldPos[3];
        for (int j = 0; j < 3; j++)
        {
            Vec3f vertexPos = model->VertexPosition(face[j].positionIndex);
            screenPos[j] = Vec3i((vertexPos.x + 1.) * image.get_width() / 2., (vertexPos.y + 1.) * image.get_height() / 2., 0);
            worldPos[j] = vertexPos;
        }

        Vec3f n = (worldPos[2] - worldPos[0]) ^ (worldPos[1] - worldPos[0]);
        n.normalize();
        float intensity = n * light_dir;
        if (intensity > 0)
            Triangle2D(screenPos[0], screenPos[1], screenPos[2], image,
                TGAColor(intensity * 255, intensity * 255, intensity * 255, 255));
    }
    delete model;
}

/*
* ********************************************
*                   STEP 7
* ********************************************
*/

// draw line using zBuffer
void LineZBuffer(Vec3f t0, Vec3f t1, TGAImage& image, const TGAColor& color, float* zBuffer)
{
    // if line is steep, transpose it
    bool steep = false;
    if (std::abs(t0.x - t1.x) < std::abs(t0.y - t1.y))
    {
        std::swap(t0.x, t0.y);
        std::swap(t1.x, t1.y);
        steep = true;
    }

    // order vertices by x ascending
    if (t0.x > t1.x)
    {
        std::swap(t0, t1);
    }

    // calculate slope
    float y = t0.y;
    float dy = (t1.x != t0.x) ? (float)(t1.y - t0.y) / (t1.x - t0.x) : 0;

    // z value goes from -1 to 1
    // clamp z from 0 to 1
    t0.z = (t0.z + 1) / 2;
    t1.z = (t1.z + 1) / 2;

    float z = t0.z;
    float dz = (t1.x != t0.x) ? (float)(t1.z - t0.z) / (t1.x - t0.x) : 0;

    // draw line pixel by pixel
    for (int x = t0.x; x <= t1.x; x++)
    {
        //index = x + y * width;
        if (z > zBuffer[x + (int)y * image.get_width()])
        {
            zBuffer[x + (int)y * image.get_width()] = z;
            if (steep)
                image.set(y + 0.5, x, TGAColor(z * 255, z * 255, z* 255, 255));
            else
                image.set(x, y + 0.5, TGAColor(z * 255, z * 255, z * 255, 255));
        }
        y += dy;
        z += dz;
    }
}

// draw triangle with zBuffer using LineZBuffer
void TriangleZBuffer(Vec3f t0, Vec3f t1, Vec3f t2, TGAImage& image, const TGAColor& color, float* zBuffer)
{
    // order vertices by y ascending 
    if (t0.y > t1.y) std::swap(t0, t1);
    if (t0.y > t2.y) std::swap(t0, t2);
    if (t1.y > t2.y) std::swap(t1, t2);

    // x starting positions
    float xShorter = t0.x;
    float xLonger = t0.x;

    // z starting positions
    float zShorter = t0.z;
    float zLonger = t0.z;

    // if t0 and t1 are same hight, change starting position to t1
    if (t0.y == t1.y)
    {
        xShorter = t1.x;
        zShorter = t1.z;
    }

    // calculate longer and shorter slope
    float alpha = (t0.y != t1.y) ? (t1.x - t0.x) / (float)(t1.y - t0.y) : 0; // shorter slope
    float beta = (t0.y != t2.y) ? (t2.x - t0.x) / (float)(t2.y - t0.y) : 0; // longer slope

    // calculate slope for z values
    float zAlpha = (t0.y != t1.y) ? (t1.z - t0.z) / (float)(t1.y - t0.y) : 0; // shorter slope
    float zBeta = (t0.y != t2.y) ? (t2.z - t0.z) / (float)(t2.y - t0.y) : 0; // longer slope

    // draw bottom half
    for (int y = t0.y; y < t1.y; y++)
    {
        LineZBuffer({ (int)(xShorter + 0.5), y, zShorter }, { (int)(xLonger + 0.5), y, zLonger }, image, color, zBuffer);

        xShorter += alpha;
        xLonger += beta;
        zShorter += zAlpha;
        zLonger += zBeta;
    }

    // calculate new shorter slope
    alpha = (t1.y != t2.y) ? (t2.x - t1.x) / (float)(t2.y - t1.y) : 0;
    zAlpha = (t1.y != t2.y) ? (t2.z - t1.z) / (float)(t2.y - t1.y) : 0;

    // draw upper half
    for (int y = t1.y; y < t2.y; y++)
    {
        LineZBuffer({ (int)(xShorter + 0.5), y, zShorter }, { (int)(xLonger + 0.5), y, zLonger }, image, color, zBuffer);

        xShorter += alpha;
        xLonger += beta;
        zShorter += zAlpha;
        zLonger += zBeta;
    }
}

// draw z-buffer representation - close=white, far=black
void DrawZBuffer(TGAImage& image, float* zBuffer, Model* model)
{
    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        std::vector<FaceStruct> face = model->Face(i);
        Vec3f screenPos[3];
        for (int j = 0; j < 3; j++)
        {
            Vec3f vertexPos = model->VertexPosition(face[j].positionIndex);

            // convert relative vertex screen position to screen position and cast to int
            int x = (int)((vertexPos.x + 1) * image.get_width() / 2 + 0.5);
            int y = (int)((vertexPos.y + 1) * image.get_height() / 2 + 0.5);
            screenPos[j] = Vec3f(x, y, vertexPos.z);
        }
        TriangleZBuffer(screenPos[0], screenPos[1], screenPos[2], image, TGAColor(0, 0, 0, 255), zBuffer);
    }
}

/*
* ********************************************
*                   STEP 8
* ********************************************
*/

void TexturedTriangle(Vec3i t[3], Vec2i uv[3], float intensity, TGAImage& image, Model* model, float* zBuffer)
{
    // order vertices by y ascending 
    if (t[0].y > t[1].y) { std::swap(t[0], t[1]); std::swap(uv[0], uv[1]); };
    if (t[0].y > t[2].y) { std::swap(t[0], t[2]); std::swap(uv[0], uv[2]); };
    if (t[1].y > t[2].y) { std::swap(t[1], t[2]); std::swap(uv[1], uv[2]); };

    int totalHeight = t[2].y - t[0].y;
    TGAColor c(rand() % 255, rand() % 255, rand() % 255, 255);
    for (int i = 0; i < totalHeight; i++)
    {
        bool secondHalf = i > t[1].y - t[0].y || t[1].y == t[0].y;
        int segmentHeight = secondHalf ? t[2].y - t[1].y : t[1].y - t[0].y;

        float alpha = (float)i / totalHeight;
        float beta = (float)(i - (secondHalf ? t[1].y - t[0].y : 0)) / segmentHeight;

        Vec3i a = t[0] + Vec3f(t[2] - t[0]) * alpha;
        Vec3i b = secondHalf ? t[1] + Vec3f(t[2] - t[1]) * beta : t[0] + Vec3f(t[1] - t[0]) * beta;

        Vec2i uvA = uv[0] + (uv[2] - uv[0]) * alpha;
        Vec2i uvB = secondHalf ? uv[1] + (uv[2] - uv[1]) * beta : uv[0] + (uv[1] - uv[0]) * beta;

        Vec3i lineEnds[2] = { a, b };
        Vec2i uv[2] = { uvA, uvB };

        if (a.x > b.x) { std::swap(a, b); std::swap(uvA, uvB); }
        for (int j = a.x; j <= b.x; j++)
        {
            float phi = b.x == a.x ? 1. : (float)(j - a.x) / (float)(b.x - a.x);
            Vec3i P = Vec3f(a) + Vec3f(b - a) * phi;
            Vec2i uvP = uvA + (uvB - uvA) * phi;
            int idx = P.x + P.y * image.get_width();
            if (zBuffer[idx] < P.z)
            {
                zBuffer[idx] = P.z;
                TGAColor color = model->TextureColor(uvP);
                image.set(P.x, P.y, color * intensity);
            }
        }
    }
}

void DrawTexturedModel(TGAImage& image, float* zBuffer, Model* model)
{
    Vec3f lightDirection(0, 0, -1);

    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        std::vector<FaceStruct> face = model->Face(i);
        Vec3i screenPos[3];
        Vec3f worldPos[3];
        for (int j = 0; j < 3; j++)
        {
            Vec3f vertexPos = model->VertexPosition(face[j].positionIndex);
            worldPos[j] = vertexPos;

            // convert relative vertex screen position to screen position and cast to int
            int x = (int)((vertexPos.x + 1) * image.get_width() / 2);
            int y = (int)((vertexPos.y + 1) * image.get_height() / 2);
            int z = (int)((vertexPos.z + 1) * image.get_height() / 2);
            screenPos[j] = Vec3i(x, y, z);
        }

        Vec3f n = (worldPos[2] - worldPos[0]) ^ (worldPos[1] - worldPos[0]);
        n.normalize();
        float intensity = n * lightDirection;
        if (intensity > 0)
        {
            Vec2i uv[3];
            for (int j = 0; j < 3; j++)
                uv[j] = model->UV(i, j);
            TexturedTriangle(screenPos, uv, intensity, image, model, zBuffer);
        }

    }
}

/*
* ********************************************
*                   STEP 9
* ********************************************
*/

void DrawModelWithPerspective(TGAImage& image, float* zBuffer, Model* model)
{
    Vec3f lightDirection(0, 0, -1);
    Vec3f camera(0, 0, 3);


    Matrix perspective = Matrix::Identity(); 
    perspective[3][2] = -1 / camera.z;
    Matrix viewPort = Matrix::ViewPort(image.get_width() / 8, image.get_height() / 8, image.get_width() * 3 / 4, image.get_height() * 3 / 4);
    Matrix transformationMatrix = viewPort * perspective;

    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        std::vector<FaceStruct> face = model->Face(i);
        Vec3i screenPos[3];
        Vec3f worldPos[3];
        for (int j = 0; j < 3; j++)
        {
            Vec3f vertexPos = model->VertexPosition(face[j].positionIndex);
            worldPos[j] = vertexPos;

            screenPos[j] = transformationMatrix * vertexPos;
        }

        Vec3f n = (worldPos[2] - worldPos[0]) ^ (worldPos[1] - worldPos[0]);
        n.normalize();
        float intensity = n * lightDirection;
        if (intensity > 0)
        {
            Vec2i uv[3];
            for (int j = 0; j < 3; j++)
                uv[j] = model->UV(i, j);
            TexturedTriangle(screenPos, uv, intensity, image, model, zBuffer);
        }

    }
}

/*
* ********************************************
*                   STEP 10
* ********************************************
*/

void DrawModelWithMovingCamera(TGAImage& image, float* zBuffer, Model* model, WorldSetting worldSettings)
{
    worldSettings.lightDirection = Vec3f(0, 0, -1);
    Vec3f cameraVector = worldSettings.eyeVector - worldSettings.center;
    Matrix projection = Matrix::ProjectionMatrix(cameraVector.norm());
    Matrix viewPort = Matrix::ViewPort(image.get_width() / 8, image.get_height() / 8, image.get_width() * 3 / 4, image.get_height() * 3 / 4);
    Matrix transformationMatrix = viewPort * projection;
    transformationMatrix = transformationMatrix * Matrix::LookAt(worldSettings.eyeVector, worldSettings.center, Vec3f::up());

    // cycle through entire object face by face
    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        // get current face
        std::vector<FaceStruct> face = model->Face(i);

        // get screen position, world positions and intensity of 3 vertices describing current face
        Vec3i screenPos[3]; // pixel positions on final screen
        Vec3f worldPos[3]; // used for calculating face normal
        Vec2i uv[3]; // uv coordinates

        // visit each vertex of current face and get information from .obj file
        for (int j = 0; j < 3; j++)
        {
            Vec3f vertexPos = model->VertexPosition(face[j].positionIndex);
            worldPos[j] = vertexPos;
            screenPos[j] = transformationMatrix * vertexPos;
            uv[j] = model->UV(i, j);
        }

        // calculate light intensity of face normal
        Vec3f faceNormal = ((worldPos[2] - worldPos[0]) ^ (worldPos[1] - worldPos[0])).normalize();
        float intensity = faceNormal * worldSettings.lightDirection;

        // if light intensitiy is =0 (face is paralel to light source),
        // or intensity is <0 (face is illuminated from the back)
        // skip current face
        if (intensity > 0)
        {
            Vec2i uv[3];
            for (int j = 0; j < 3; j++)
                uv[j] = model->UV(i, j);
            TexturedTriangle(screenPos, uv, intensity, image, model, zBuffer);
        }

    }
}

/*
* ********************************************
*                   STEP 11
* ********************************************
*/

void TriangleGouraudShading(Vec3i t[3], Vec2i uv[3], float intensity[3], TGAImage& image, Model* model, float* zBuffer)
{
    // order vertices by y ascending 
    if (t[0].y > t[1].y) { std::swap(t[0], t[1]); std::swap(uv[0], uv[1]); std::swap(intensity[0], intensity[1]); };
    if (t[0].y > t[2].y) { std::swap(t[0], t[2]); std::swap(uv[0], uv[2]); std::swap(intensity[0], intensity[2]); };
    if (t[1].y > t[2].y) { std::swap(t[1], t[2]); std::swap(uv[1], uv[2]); std::swap(intensity[1], intensity[2]); };

    // height of the segment with y difference
    int totalHeight = t[2].y - t[0].y;
    // height of the bottom part
    int firstSegmentHeight = t[1].y - t[0].y;
    // height of the upper part
    int secondSegmentHeight = t[2].y - t[1].y;

    // variables that keep track of current half
    int currentSegmentHeight = firstSegmentHeight;
    int segmentIndex = 0; // 0 if bottom, 1 if upper
    int lineNumber = 0; // line number in current part

    // draw triangle line by line
    for (int i = 0; i < totalHeight; i++)
    {
        // check if first segment is completed
        if (i == firstSegmentHeight)
        {
            currentSegmentHeight = secondSegmentHeight;
            lineNumber = 0;
            segmentIndex = 1;
        }

        // current percentage for difference vector
        float alpha = (float)i / totalHeight;
        float beta = (float)lineNumber / currentSegmentHeight;
        lineNumber++;

        // current line begining and ending
        // starting vector + difference vector * percentage
        // difference vector = ending vector - starting vector
        Vec3i a = t[0] + Vec3f(t[2] - t[0]) * alpha;
        Vec3i b = t[segmentIndex] + Vec3f(t[segmentIndex + 1] - t[segmentIndex]) * beta;

        // texture coodrinates for line begining and ending
        Vec2i uvA = uv[0] + (uv[2] - uv[0]) * alpha;
        Vec2i uvB = uv[segmentIndex] + (uv[segmentIndex + 1] - uv[segmentIndex]) * beta;

        // intensity for line begining and ending
        float intensityA = intensity[0] + (intensity[2] - intensity[0]) * alpha;
        float intensityB = intensity[segmentIndex] + (intensity[segmentIndex + 1] - intensity[segmentIndex]) * beta;

        // order vertices by x ascending
        if (a.x > b.x) { std::swap(a, b); std::swap(uvA, uvB); std::swap(intensityA, intensityB); }
        int lineWidth = b.x - a.x;
        // draw horizontal line pixel by pixel
        for (int j = 0; j <= lineWidth; j++)
        {
            // current percentage for difference vector
            float gama = (j == lineWidth) ? 1 : (float)j / lineWidth;

            // current pixel position, uv coodrinates and intensity
            // current = starting + difference * percentage
            // difference = end - sart
            Vec3i pixelPosition = Vec3f(a) + Vec3f(b - a) * gama;
            Vec2i pixelUv = uvA + (uvB - uvA) * gama;
            float pixelIntensity = intensityA + (intensityB - intensityA) * gama;

            // skip pixel out of bound
            if (pixelPosition.x >= image.get_width() || pixelPosition.x < 0 ||
                pixelPosition.y >= image.get_height() || pixelPosition.y < 0)
                continue;

            // calculate 1D array index form 2D coordinates
            int zBufferIndex = pixelPosition.x + pixelPosition.y * image.get_width();

            // if current pixel is in front of existing pixel (or on empty location), draw on top of it
            if (zBuffer[zBufferIndex] < pixelPosition.z)
            {
                zBuffer[zBufferIndex] = pixelPosition.z;
                image.set(pixelPosition.x, pixelPosition.y, TGAColor(255, 255, 255) * pixelIntensity);
            }
        }
    }
}

void DrawModelGouraudShading(TGAImage& image, float* zBuffer, Model* model, WorldSetting worldSettings)
{
    Vec3f cameraVector = worldSettings.eyeVector - worldSettings.center;
    Matrix projection = Matrix::ProjectionMatrix(cameraVector.norm());
    Matrix viewPort = Matrix::ViewPort(image.get_width() / 8, image.get_height() / 8, image.get_width() * 3 / 4, image.get_height() * 3 / 4);
    Matrix transformationMatrix = viewPort * projection;
    transformationMatrix = transformationMatrix * Matrix::LookAt(worldSettings.eyeVector, worldSettings.center, Vec3f::up());

    // cycle through entire object face by face
    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        // get current face
        std::vector<FaceStruct> face = model->Face(i);

        // get screen position, world positions and intensity of 3 vertices describing current face
        Vec3i screenPos[3]; // pixel positions on final screen
        Vec3f worldPos[3]; // used for calculating face normal
        Vec2i uv[3]; // uv coordinates
        float intensity[3]; // intensity for each vertex

        // visit each vertex of current face and get information from .obj file
        for (int j = 0; j < 3; j++)
        {
            Vec3f vertexPos = model->VertexPosition(face[j].positionIndex);
            worldPos[j] = vertexPos;
            screenPos[j] = transformationMatrix * vertexPos;
            intensity[j] = model->VertexNormal(i, j) * worldSettings.lightDirection;
            uv[j] = model->UV(i, j);
        }
        TriangleGouraudShading(screenPos, uv, intensity, image, model, zBuffer);
    }
}

/*
* ********************************************
*                   STEP 12
* ********************************************
*/

// draw triangle line by line
void TriangleNormalisedTexture(Vec3i t[3], Vec2i uv[3], float intensity[3], TGAImage& image, Model* model, float* zBuffer)
{
    // order vertices by y ascending 
    if (t[0].y > t[1].y) { std::swap(t[0], t[1]); std::swap(uv[0], uv[1]); std::swap(intensity[0], intensity[1]); };
    if (t[0].y > t[2].y) { std::swap(t[0], t[2]); std::swap(uv[0], uv[2]); std::swap(intensity[0], intensity[2]); };
    if (t[1].y > t[2].y) { std::swap(t[1], t[2]); std::swap(uv[1], uv[2]); std::swap(intensity[1], intensity[2]); };

    // height of the segment with y difference
    int totalHeight = t[2].y - t[0].y;
    // height of the bottom part
    int firstSegmentHeight = t[1].y - t[0].y;
    // height of the upper part
    int secondSegmentHeight = t[2].y - t[1].y;

    // variables that keep track of current half
    int currentSegmentHeight = firstSegmentHeight;
    int segmentIndex = 0; // 0 if bottom, 1 if upper
    int lineNumber = 0; // line number in current part

    // draw triangle line by line
    for (int i = 0; i < totalHeight; i++)
    {
        // check if first segment is completed
        if (i == firstSegmentHeight)
        {
            currentSegmentHeight = secondSegmentHeight;
            lineNumber = 0;
            segmentIndex = 1;
        }

        // current percentage for difference vector
        float alpha = (float)i / totalHeight;
        float beta = (float)lineNumber / currentSegmentHeight;
        lineNumber++;

        // current line begining and ending
        // starting vector + difference vector * percentage
        // difference vector = ending vector - starting vector
        Vec3i a = t[0] + Vec3f(t[2] - t[0]) * alpha;
        Vec3i b = t[segmentIndex] + Vec3f(t[segmentIndex + 1] - t[segmentIndex]) * beta;

        // texture coodrinates for line begining and ending
        Vec2i uvA = uv[0] + (uv[2] - uv[0]) * alpha;
        Vec2i uvB = uv[segmentIndex] + (uv[segmentIndex + 1] - uv[segmentIndex]) * beta;

        // intensity for line begining and ending
        float intensityA = intensity[0] + (intensity[2] - intensity[0]) * alpha;
        float intensityB = intensity[segmentIndex] + (intensity[segmentIndex + 1] - intensity[segmentIndex]) * beta;

        // order vertices by x ascending
        if (a.x > b.x) { std::swap(a, b); std::swap(uvA, uvB); std::swap(intensityA, intensityB); }
        int lineWidth = b.x - a.x;
        // draw horizontal line pixel by pixel
        for (int j = 0; j <= lineWidth; j++)
        {
            // current percentage for difference vector
            float gama = (j == lineWidth) ? 1 : (float)j / lineWidth;

            // current pixel position, uv coodrinates and intensity
            // current = starting + difference * percentage
            // difference = end - sart
            Vec3i pixelPosition = Vec3f(a) + Vec3f(b - a) * gama;
            Vec2i pixelUv = uvA + (uvB - uvA) * gama;
            float pixelIntensity = intensityA + (intensityB - intensityA) * gama;

            // skip pixel out of bound
            if (pixelPosition.x >= image.get_width() || pixelPosition.x < 0 ||
                pixelPosition.y >= image.get_height() || pixelPosition.y < 0)
                continue;

            // calculate 1D array index form 2D coordinates
            int zBufferIndex = pixelPosition.x + pixelPosition.y * image.get_width();

            // if current pixel is in front of existing pixel (or on empty location), draw on top of it
            if (zBuffer[zBufferIndex] < pixelPosition.z)
            {
                zBuffer[zBufferIndex] = pixelPosition.z;
                TGAColor pixelColor = model->TextureColor(pixelUv);
                image.set(pixelPosition.x, pixelPosition.y, pixelColor * pixelIntensity);
            }
        }
    }
}


void DrawModelNormalisedTexture(TGAImage& image, float* zBuffer, Model* model, WorldSetting worldSettings)
{
    Vec3f cameraVector = worldSettings.eyeVector - worldSettings.center;
    Matrix projection = Matrix::ProjectionMatrix(cameraVector.norm());
    Matrix viewPort = Matrix::ViewPort(image.get_width() / 8, image.get_height() / 8, image.get_width() * 3 / 4, image.get_height() * 3 / 4);
    Matrix transformationMatrix = viewPort * projection;
    transformationMatrix = transformationMatrix * Matrix::LookAt(worldSettings.eyeVector, worldSettings.center, Vec3f::up());

    // cycle through entire object face by face
    for (int i = 0; i < model->NumberOfFaces(); i++)
    {
        // get current face
        std::vector<FaceStruct> face = model->Face(i);

        // get screen position, world positions and intensity of 3 vertices describing current face
        Vec3i screenPos[3]; // pixel positions on final screen
        Vec3f worldPos[3]; // used for calculating face normal
        Vec2i uv[3]; // uv coordinates
        float intensity[3]; // intensity for each vertex

        // visit each vertex of current face and get information from .obj file
        for (int j = 0; j < 3; j++)
        {
            Vec3f vertexPos = model->VertexPosition(face[j].positionIndex);
            worldPos[j] = vertexPos;
            screenPos[j] = transformationMatrix * vertexPos;
            intensity[j] = model->VertexNormal(i, j) * worldSettings.lightDirection;
            uv[j] = model->UV(i, j);
        }
        TriangleNormalisedTexture(screenPos, uv, intensity, image, model, zBuffer);
    }
}