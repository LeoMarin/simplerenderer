#pragma once

#include <cmath>

template <class t> struct Vec3
{
	t x, y, z;

	Vec3() : x(0), y(0), z(0) {}
	Vec3(t _x, t _y, t _z) : x(_x), y(_y), z(_z) {}
	Vec3(int _x, int _y, float _z) : x((t)_x), y((t)_y), z((t)_z) {}

	template <typename tn>
	Vec3(Vec3<tn> v)
	{
		if (v.x > (int)v.x)
			x = v.x + 0.5;
		else
			x = v.x;

		if (v.y > (int)v.y)
			y = v.y + 0.5;
		else
			y = v.y;

		if (v.z > (int)v.z)
			z = v.z + 0.5;
		else
			z = v.z;
	}

	inline Vec3<t> operator ^(const Vec3<t>& v) const { return Vec3<t>(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x); }
	inline Vec3<t> operator +(const Vec3<t>& v) const { return Vec3<t>(x + v.x, y + v.y, z + v.z); }
	inline Vec3<t> operator -(const Vec3<t>& v) const { return Vec3<t>(x - v.x, y - v.y, z - v.z); }
	inline Vec3<t> operator *(float f)          const { return Vec3<t>(x * f, y * f, z * f); }
	inline Vec3<t> operator /(float f)          const { return Vec3<t>(x / f, y / f, z / f); }
	inline t       operator *(const Vec3<t>& v) const { return x * v.x + y * v.y + z * v.z; }
	inline float operator[](int i)
	{
		switch (i)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		default:
			break;
		}
	}
	float norm() const { return std::sqrt(x * x + y * y + z * z); }
	Vec3<t>& normalize(t l = 1) { *this = (*this) * (l / norm()); return *this; }
	template <class > friend std::ostream& operator<<(std::ostream& s, Vec3<t>& v);
	static Vec3<t> up() { return Vec3<t>(0, 1, 0); }
};

template <class t> struct Vec2
{
	t x, y;
	Vec2() : x(0), y(0) {}
	Vec2(t _x, t _y) : x(_x), y(_y) {}
	Vec2(Vec3<t> vec3) : x(vec3.x), y(vec3.y) {};

	template <typename tn>
	Vec2(Vec2<tn> v)
	{
		if (v.x > (int)v.x || v.y > (int)v.y)
		{
			// conversion from float to int
			x = v.x + 0.5;
			y = v.y + 0.5;

		}
		else
		{
			// conversion from int to float
			x = v.x;
			y = v.y;

		}
	}

	inline Vec2<t> operator +(const Vec2<t>& V) const { return Vec2<t>(x + V.x, y + V.y); }
	inline Vec2<t> operator -(const Vec2<t>& V) const { return Vec2<t>(x - V.x, y - V.y); }
	inline Vec2<t> operator *(float f)          const { return Vec2<t>(x * f, y * f); }
	inline Vec2<t> operator /(float f)          const { return Vec2<t>(x / f, y / f); }
	template <class > friend std::ostream& operator<<(std::ostream& s, Vec2<t>& v);
};

typedef Vec2<float> Vec2f;
typedef Vec2<int>   Vec2i;
typedef Vec3<float> Vec3f;
typedef Vec3<int>   Vec3i;

template <class t> std::ostream& operator<<(std::ostream& s, Vec2<t>& v)
{
	s << "(" << v.x << ", " << v.y << ")";
	return s;
}

template <class t> std::ostream& operator<<(std::ostream& s, Vec3<t>& v)
{
	s << "(" << v.x << ", " << v.y << ", " << v.z << ")";
	return s;
}
