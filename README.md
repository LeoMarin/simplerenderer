Simple renderer using only pixel coloring functionality from TGA class.

[[_TOC_]]

## Step 0: Coloring pixels and reading .obj file

Using single function from TGA class `bool TGAImage::set(int x, int y, TGAColor c)` we can color in one pixel.

![single pixel](SimpleRenderer/images/single_pixel.png)

To render a model on screen we need to read information stored in .obj file.\
.obj files contain 4 types of information:
1. vertex postions
2. uv coordinates
3. vertex normals
4. faces

Lines containing vertex positions look like:\
`v [float] [float] [float]`\
Floats go from -1 to 1 and represent relative position of each vertex.

Lines containing uv coodrinates look like:\
`vt [float] [float] 0.000`\
Floats go from 0 to 1 and represent relative coordinates on uv texture.

Lines containing vertex normals look like:\
`vt [float] [float] [float]`\
Floats go from -1 to 1 and represent normal vector of a given vertex.

Lines containing faces look like:\
`f [int]/[int]/[int] [int]/[int]/[int] [int]/[int]/[int] `\
Each line consist of information for 3 vertices that define a gvien face. 
First number represents index of vertex coordinate, second number represents 
index of uv coordinates and the thrid number represents index of vertex normals.\
Note: indices start from 1, not 0

## Step 1: Drawing lines

Draw lines using Bresenhamís line drawing algorithm

![lines](SimpleRenderer/images/lines.png)

## Step 2: Wireframe model using line segments

![wireframe model](SimpleRenderer/images/wireframe.png)

## Step 3: Draw wire triangles

![wire triangles](SimpleRenderer/images/wire_triangles.png)

## Step 4: Color in triangles

![colored tirangles](SimpleRenderer/images/colored_triangles.png)

## Step 5: Flat shaded (random colors) model

![flat shaded model](SimpleRenderer/images/flat_shaded_model.png)

## Step 6: Illuminated model

![illuminated model](SimpleRenderer/images/illuminated_model.png)

## Step 7: ZBuffer

![z-buffer visualisation](SimpleRenderer/images/z_buffer_visualisation.png)

## Step 8: Applying texture

![textured model](SimpleRenderer/images/textured_model.png)

## Step 9: Perspective projection

![perspctive projection](SimpleRenderer/images/perspective_projection.png)

## Step 10: Moving the camera

![moving the camera](SimpleRenderer/images/moving_the_camera.png)

## Step 11: Gouraud shading

![Gouraud shading](SimpleRenderer/images/gouraud_shading.png)

## Step 12: Normalised texture

![normalised texture](SimpleRenderer/images/normalised_texture.png)
